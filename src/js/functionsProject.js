//Fechar nav ao clicar
$('.navbar-nav>li>a').on('click', function(){
    $('.navbar-collapse').collapse('hide');
});

//Scroll Page
$('a.scroll_section').on('click',function (e) {
    e.preventDefault();
    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top -10
    }, 2500, 'swing', function () {
     window.location.hash = target;
    });
});

//slick menu
$(window).on('scroll',function() {
  var scrolltop = $(this).scrollTop();

  if(scrolltop >= 40) {
    $('.navClient').addClass('fixClient');
  }

  else if(scrolltop <= 40) {
    $('.navClient').removeClass('fixClient');
  }

  if(scrolltop >= 1200) {
      $(".animated").addClass("fadeInUp");
  } 

});

$(document).on( "click", '.min-video', function(e) {
  $('#modal-conteudo').modal('show');
});


//stop video ao fechar modal
$('#modal-conteudo').on('hide.bs.modal', function(e) {    
    var $if = $(e.delegateTarget).find('iframe');
    var src = $if.attr("src");
    $if.attr("src", '/empty.html');
    $if.attr("src", src);
});
