$("#login").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Preencha todos os campos!");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var email_login = $("#email_login").val();
    var senha_login = $("#senha_login").val();

    $.ajax({
        type: "POST",
        url: "send_inscricao.php",
        data: "email_login=" + email_login + "senha_login=" + senha_login,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#login")[0].reset();
    submitMSG(true, "Mensagem enviada com sucesso!")
}

function formError(){
    $("#login").removeClass().addClass('animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#retorno").removeClass().addClass(msgClasses).text(msg);
}