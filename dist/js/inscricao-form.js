$("#cadastro").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Você precisa preencher o formulário corretamente");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var nome = $("#nome").val();
    var email = $("#email").val();
    var ccpf = $("#cpf").val();    
    var telefone = $("#telefone").val();
    var area_inter = $("#area_inter").val();
    var curs_inter = $("#curs_inter").val();
    var colegio = $("#colegio").val();
    var usuario = $("#senha").val();


    $.ajax({
        type: "POST",
        url: "send_inscricao.php",
        data: "nome=" + nome + "&email=" + email + "&cpf=" + cpf + "&telefone=" + telefone + "&area_inter=" + area_inter + "&curs_inter=" + curs_inter + "&colegio=" + colegio + "&usuario=" + usuario + "&senha=" + senha,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#cadastro")[0].reset();
    submitMSG(true, "Mensagem enviada com sucesso!")
}

function formError(){
    $("#cadastro").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}