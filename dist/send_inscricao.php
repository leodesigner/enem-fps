<?php

$errorMSG = "";

// NAME
if (empty($_POST["nome"])) {
    $errorMSG = "Insira um nome ";
} else {
    $nome = $_POST["nome"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Insira um E-mail";
} else {
    $email = $_POST["email"];
}

// TELEFONNE
if (empty($_POST["telefone"])) {
    $errorMSG .= "Insira um Telefone";
} else {
    $telefone = $_POST["telefone"];
}

// CPF
if (empty($_POST["cpf"])) {
    $errorMSG .= "Insira seu CPF";
} else {
    $cpf = $_POST["cpf"];
}

// ÁREA DE INTERESSE
if (empty($_POST["area_inter"])) {
    $errorMSG .= "Selecione uma Área de interesse";
} else {
    $area_inter = $_POST["area_inter"];
}

// CURSO DE INTERESSE
if (empty($_POST["curs_inter"])) {
    $errorMSG .= "Selecione o Curso de interesse";
} else {
    $curs_inter = $_POST["curs_inter"];
}

// COLEGIO
if (empty($_POST["colegio"])) {
    $errorMSG .= "Insira o nome do Colégio";
} else {
    $colegio = $_POST["colegio"];
}

// USUARIO
if (empty($_POST["usuario"])) {
    $errorMSG .= "Insira o nome do Usuario";
} else {
    $usuario = $_POST["usuario"];
}

// SENHA
if (empty($_POST["senha"])) {
    $errorMSG .= "Insira uma Senha";
} else {
    $senha = $_POST["senha"];
}


$EmailTo = "email-enem@fps.edu.br";
$Subject = "Formulário de Inscrição ENEM - FBV";

// prepare email body text
$Body = "";
$Body .= "Nome: ";
$Body .= $nome;
$Body .= "\n";
$Body .= "E-mail: ";
$Body .= $email;
$Body .= "\n";
$Body .= "CPF: ";
$Body .= $cpf;
$Body .= "\n";
$Body .= "Telefonr: ";
$Body .= $telefone;
$Body .= "\n";
$Body .= "Área de Interesse: ";
$Body .= $area_inter;
$Body .= "\n";
$Body .= "Curso de Interesse: ";
$Body .= $curs_inter;
$Body .= "\n";
$Body .= "Colégio: ";
$Body .= $colegio;
$Body .= "\n";
$Body .= "Usuário: ";
$Body .= $usuario;
$Body .= "\n";
$Body .= "Senha: ";
$Body .= $senha;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>